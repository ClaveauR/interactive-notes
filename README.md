# Interactive Notes

Interactive Notes is a web project based on an idea, the possibility to insert dynamism into a document, an article or a course.

For this purpose, every document is considered as a stack of interactive components, called modules. Each module can be edited, personalized individually, to meet all your requirements.

You can also collaborate with your colleagues or friends using collaborative tools like real-time edition, instantaneous discussion, tasks manager, etc.

Once your project complete, you may share it as a static or dynamic document.

This project can help you in creating more appealing documents for your superiors, friends, students or even yourself.

In addition to providing a free-to-use editor, Interactive Notes also offers a way to broadcast knowledge with the creation of a free document database.

This project is part of an environmentally friendly approach by intending to offset our carbon footprint. For that, a portion of the donations (10%) is going directly to an environmental association that plants trees.

For more information, you can contact me using the following email address: romain [DOT] claveau [AT] protonmail [DOT] com

We look forward to receiving your feedback on the project.


- Romain