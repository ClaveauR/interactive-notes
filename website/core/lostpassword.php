<?php
    session_start();

    define('APP_RAN', 'APP_RAN');

    header('Content-Type: text/html; charset=utf-8');

    require_once("database.php");

    if(isset($_POST["mail"]) && !empty($_POST["mail"]))
    {
        $database = databaseConnection();
        
        $req = $database->prepare("SELECT * FROM users WHERE mail = ?");
        $req->execute(array($_POST["mail"]));
        $result = $req->fetchAll();
        
        $userId = $result[0]["id"];
        
        $req->closeCursor();
        
        if(count($result) == 1)
        {
            $mail_token = hash("sha512", md5(microtime()));
            
            $req = $database->prepare("INSERT INTO newpassword (id, userId, token) VALUES (?, ?, ?)");
            $req->execute(array(null, $userId, $mail_token));
            $req->closeCursor();    
            
            $mail_to = $_POST["mail"];
            $mail_subject = "Redéfinition de votre mot de passe.";
            $mail_headers = "From: noreply@interactive-notes.fr\r\nX-Mailer: PHP/" . phpversion();
            $mail_message = "
                Vous avez demandé un réinitialisation de votre mot de passe sur Interactive Notes. Avant toute action de votre part, nous attirons votre attention sur le fait que <b>vos documents sont chiffrés à l'aide de votre mot de passe et que donc toute réinitialisation entrainera la perte de ces documents</b>.<br /><br />
                Pour réinitialiser votre mot de passe, suivez le lien suivant :<br />
                <a href='https://interactive-notes.fr/website/newpassword.php?token={$mail_token}'>https://interactive-notes.fr/website/newpassword.php?token={$mail_token}</a><br /><br /><br />
                L'équipe d'Interactive Notes
            ";
            
            mail($mail_to, $mail_subject, $mail_message, $mail_headers);
            
            die("OK");
        }
    }
?>