<?php
    session_start();

    $status = "OPEN";

    define('APP_RAN', 'APP_RAN');

    header('Content-Type: text/html; charset=utf-8');

    require_once("database.php");

    if(isset($_POST["mail"]) && !empty($_POST["mail"]) && isset($_POST["password"]) && !empty($_POST["password"]) && isset($_POST["password_confirm"]) && !empty($_POST["password_confirm"]))
    {
        if($status == "CLOSE") die("REGISTER_CLOSE");
        if($_POST["password"] != $_POST["password_confirm"]) die("PASSWORD_DIFFERENT");
        if(filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL) === false) die("MAIL_FORMAT");
        
        $database = databaseConnection();
        $database->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        
        $req = $database->prepare("SELECT * FROM users WHERE mail = ?");
        $req->execute(array($_POST["mail"]));
        $result = $req->fetchAll();
        $req->closeCursor();
        
        if(count($result) == 0)
        {            
            $req = $database->prepare("INSERT INTO users (id, pseudo, mail, password) VALUES (?, ?, ?, ?)");
            $req->execute(array(null, null, $_POST["mail"], $_POST["password"]));
            $lastID = $database->lastInsertId();
            
            $_SESSION["user"] = array(
                "id" => $lastID,
                "mail" => $_POST["mail"],
                "pseudo" => null
            );
            
            die("OK");
        }
        
        if(count($result) == 1) die("MAIL_EXISTS");
    }
?>