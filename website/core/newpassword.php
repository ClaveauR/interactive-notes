<?php
    session_start();

    define('APP_RAN', 'APP_RAN');

    header('Content-Type: text/html; charset=utf-8');

    require_once("database.php");

    if(isset($_POST["token"]) && !empty($_POST["token"]) && isset($_POST["password"]) && !empty($_POST["password"]) && isset($_POST["password_confirm"]) && !empty($_POST["password_confirm"]))
    {
        if($_POST["password"] != $_POST["password_confirm"]) die("PASSWORD_DIFFERENT");
        
        $database = databaseConnection();
        
        $req = $database->prepare("SELECT * FROM newpassword WHERE token = ?");
        $req->execute(array($_POST["token"]));
        $result = $req->fetchAll();
        $req->closeCursor();
        
        if(count($result) == 1)
        {
            $userId = $result[0]["userId"];
            $id = $result[0]["id"];
            
            $req = $database->prepare("UPDATE users SET password = ? WHERE id = ?");
            $req->execute(array($_POST["password"], $userId));
            $req->closeCursor();
            
            $req = $database->prepare("DELETE FROM newpassword WHERE id = ?");
            $req->execute(array($id));
            $req->closeCursor();
            
            die("OK");
        }
    }
?>