<?php
    defined('APP_RAN') or die("ERROR::CORE::ACCESS::FUNCTION::DIRECT");

    function databaseConnection()
    {
        $db_config = array(
            "SGBD" => "mysql",
            "HOST" => "localhost",
            "DB_NAME" => "interactive-notes",
            "USER" => "root",
            "PASSWORD" => "",
            "CHARSET" => "utf8",
            "PORT" => 3306,
            "OPTIONS" => array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            )
        );

        try
        {
            $database = new \PDO("{$db_config['SGBD']}:dbname={$db_config['DB_NAME']};host={$db_config['HOST']};charset={$db_config['CHARSET']};port={$db_config['PORT']}", "{$db_config['USER']}", "{$db_config['PASSWORD']}", $db_config['OPTIONS']);
        }
        catch(Exception $error)
        {
            trigger_error($error->getMessage(), E_USER_ERROR);
        }
        
        return $database;
    }
?>