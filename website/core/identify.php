<?php
    session_start();

    define('APP_RAN', 'APP_RAN');

    header('Content-Type: text/html; charset=utf-8');

    require_once("database.php");

    if(isset($_POST["mail"]) && !empty($_POST["mail"]) && isset($_POST["password"]) && !empty($_POST["password"]))
    {
        $database = databaseConnection();
        
        $req = $database->prepare("SELECT * FROM users WHERE mail = ? AND password = ?");
        $req->execute(array($_POST["mail"], $_POST["password"]));
        $result = $req->fetchAll();
        $req->closeCursor();
        
        if(count($result) == 1)
        {
            $_SESSION["user"] = array(
                "id" => $result[0]["id"],
                "mail" => $result[0]["mail"],
                "pseudo" => $result[0]["pseudo"]
            );
            
            die("OK");
        }
    }
?>