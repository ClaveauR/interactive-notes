<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Interactive Notes</title>
        <link rel="icon" type="image/png" href="public/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="public/css/general.css" />
        <link rel="stylesheet" type="text/css" href="public/css/index.css" />
        <link rel="stylesheet" type="text/css" href="public/css/header.css" />
        <link rel="stylesheet" type="text/css" href="public/css/main.css" />
        <link rel="stylesheet" type="text/css" href="public/css/section_about.css" />
        <link rel="stylesheet" type="text/css" href="public/css/form.css" />
        <script type="text/javascript" src="public/js/core.js"></script>
        <script type="text/javascript" src="public/js/min.js"></script>
        <script type="text/javascript" src="public/js/librairies/sha512.js"></script>
    </head>

    <body>
        <header>
            <div>
                <span><a href="index.php">Accueil</a></span>
                <span><a href="about.php">Notre vision</a></span>
                <span><a href="features.php">Fonctionnalités</a></span>
            </div>
            <div>
            </div>
            <div>
                <span><a href="donate.php">Faire un don</a></span>
                <span><a href="versions.php">Versions</a></span>
                <span class="current"><a href="login.php">Se connecter</a></span>
            </div>
        </header>

        <main>
            <span>S'inscrire<b>|</b></span>
        </main>

        <section>
            <span>
                Vous n'êtes qu'à un pas de l'interactivité !<br /><br /><br />
                <input type="text" placeholder="Entrez votre adresse mail" /><br /><br />
                <input type="password" placeholder="Entrez votre mot de passe" /><br /><br />
                <input type="password" placeholder="Retapez votre mot de passe" /><br /><br />
                <a href="login.php">Déjà inscrit(e) ?</a><br /><br />
                <input type="button" value="S'inscrire" onclick="website.login.register();" />
                <p></p>
            </span>
        </section>

        <script type="text/javascript">
            window.onload = function()
            {
                website.about.animation();
            };
        </script>
    </body>
</html>