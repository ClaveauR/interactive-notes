<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Interactive Notes</title>
        <link rel="icon" type="image/png" href="public/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="public/css/general.css" />
        <link rel="stylesheet" type="text/css" href="public/css/index.css" />
        <link rel="stylesheet" type="text/css" href="public/css/header.css" />
        <link rel="stylesheet" type="text/css" href="public/css/main.css" />
        <link rel="stylesheet" type="text/css" href="public/css/section_about.css" />
        <script type="text/javascript" src="public/js/core.js"></script>
        <script type="text/javascript" src="public/js/min.js"></script>
    </head>

    <body>
        <header>
            <div>
                <span><a href="index.php">Accueil</a></span>
                <span><a href="about.php">Notre vision</a></span>
                <span><a href="features.php">Fonctionnalités</a></span>
            </div>
            <div>
            </div>
            <div>
                <span class="current"><a href="donate.php">Faire un don</a></span>
                <span><a href="versions.php">Versions</a></span>
                <span><a href="login.php">Se connecter</a></span>
            </div>
        </header>

        <main>
            <span>Faire un don<b>|</b></span>
        </main>

        <section>
            <span>
                Interactive Notes est un projet dont le financement repose uniquement sur des dons. Ainsi, si le projet vous intéresse et que vous avez envie de le supporter, vous pouvez donner via la plateforme Patreon :<br /><br />
                <p style="text-align: center;"><a href="https://www.patreon.com/interactive_notes">https://www.patreon.com/interactive_notes</a></p><br /><br />
                Soyez cependant averti(e) qu'il n'y a pas contrepartie, le projet étant entièrement gratuit. Considérez ainsi votre geste comme désintéressé.
            </span>
        </section>

        <script type="text/javascript">
            window.onload = function()
            {
                website.about.animation();
            };
        </script>
    </body>
</html>