<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Interactive Notes</title>
        <link rel="icon" type="image/png" href="public/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="public/css/general.css" />
        <link rel="stylesheet" type="text/css" href="public/css/index.css" />
        <link rel="stylesheet" type="text/css" href="public/css/header.css" />
        <link rel="stylesheet" type="text/css" href="public/css/main.css" />
        <link rel="stylesheet" type="text/css" href="public/css/nav.css" />
        <link rel="stylesheet" type="text/css" href="public/css/section.css" />
        <script type="text/javascript" src="public/js/core.js"></script>
        <script type="text/javascript" src="public/js/min.js"></script>
    </head>

    <body>
        <header>
            <div>
                <span class="current"><a href="index.php">Accueil</a></span>
                <span><a href="about.php">Notre vision</a></span>
                <span><a href="features.php">Fonctionnalités</a></span>
            </div>
            <div>
            </div>
            <div>
                <span><a href="donate.php">Faire un don</a></span>
                <span><a href="versions.php">Versions</a></span>
                <span><a href="login.php">Se connecter</a></span>
            </div>
        </header>

        <main>
            <span>Donnez vie à vos <i>documents</i><b>|</b></span>
        </main>

        <nav>
            <span><a href="features.php"><input type="button" value="Découvrez Interactive Notes" /></a></span>
        </nav>

        <section>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </section>

        <script type="text/javascript">
            window.onload = function()
            {
                website.index.animation();
            };
        </script>
    </body>
</html>