<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Interactive Notes</title>
        <link rel="icon" type="image/png" href="public/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="public/css/general.css" />
        <link rel="stylesheet" type="text/css" href="public/css/index.css" />
        <link rel="stylesheet" type="text/css" href="public/css/header.css" />
        <link rel="stylesheet" type="text/css" href="public/css/main.css" />
        <link rel="stylesheet" type="text/css" href="public/css/section_about.css" />
        <script type="text/javascript" src="public/js/core.js"></script>
        <script type="text/javascript" src="public/js/min.js"></script>
    </head>

    <body>
        <header>
            <div>
                <span><a href="index.php">Accueil</a></span>
                <span><a href="about.php">Notre vision</a></span>
                <span><a href="features.php">Fonctionnalités</a></span>
            </div>
            <div>
            </div>
            <div>
                <span><a href="donate.php">Faire un don</a></span>
                <span class="current"><a href="versions.php">Versions</a></span>
                <span><a href="login.php">Se connecter</a></span>
            </div>
        </header>

        <main>
            <span>Versions<b>|</b></span>
        </main>

        <section>
            <span>
                La dernière version en date d'Interactive Notes est la version <b>&alpha;lpha</b>, sortie le <b>06/11/2019</b> et comprend les changements suivants :<br /><br />
                <ul>
                    <li><b>AJOUTS</b>
                        <ul>
                            <li>Site web de présentation</li>
                            <li>Fonctionnalités de base</li>
                        </ul>
                    </li><br />
                    <li><b>AMELIORATIONS</b>
                        <ul><li>Néant</li></ul>
                    </li><br />
                    <li><b>CORRECTIONS</b>
                        <ul><li>Néant</li></ul>
                    </li>
                </ul>
            </span>
        </section>

        <script type="text/javascript">
            window.onload = function()
            {
                website.about.animation();
            };
        </script>
    </body>
</html>