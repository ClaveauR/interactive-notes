<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Interactive Notes</title>
        <link rel="icon" type="image/png" href="public/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="public/css/general.css" />
        <link rel="stylesheet" type="text/css" href="public/css/index.css" />
        <link rel="stylesheet" type="text/css" href="public/css/header.css" />
        <link rel="stylesheet" type="text/css" href="public/css/main.css" />
        <link rel="stylesheet" type="text/css" href="public/css/section_about.css" />
        <script type="text/javascript" src="public/js/core.js"></script>
        <script type="text/javascript" src="public/js/min.js"></script>
    </head>

    <body>
        <header>
            <div>
                <span><a href="index.php">Accueil</a></span>
                <span class="current"><a href="about.php">Notre vision</a></span>
                <span><a href="features.php">Fonctionnalités</a></span>
            </div>
            <div>
            </div>
            <div>
                <span><a href="donate.php">Faire un don</a></span>
                <span><a href="versions.php">Versions</a></span>
                <span><a href="login.php">Se connecter</a></span>
            </div>
        </header>

        <main>
            <span>Notre vision<b>|</b></span>
        </main>

        <section>
            <span>
                Interactive Notes est un projet né dans le but d'apporter de la vie et de l'interactivité dans les documents, notamment scientifiques. Cette interactivité est fondamentale afin de transmettre convenablement les idées, et théories véhiculées. En sciences, et surtout dans les articles scientifiques disponibles en ligne, il semble dommage de ne pas pouvoir interagir avec les différents graphiques, etc. afin de s'approprier plus facilement le contenu de l'article. L'appropriation de cette connaissance est fondamentale car elle permet ensuite la rédaction de documents, d'articles, de cours de meilleure qualité, plus clairs, plus précis, et toujours plus riches.<br /><br />

                Ce projet milite aussi pour un meilleur partage de la connaissance en ligne, car nous estimons qu'elle doit être disponible à toute personne curieuse, dérireuse d'apprendre, avide de connaissances. Ce meilleur partage permet à la fois une meilleure éducation des populations, une barrière contre les différentes manipulations, mais aussi une progression de la science en général par la vulgarisation et l'accessibilité : plus de scientifiques mieux formés = travaux plus nombreux et de meilleure qualité.
            </span>
        </section>

        <script type="text/javascript">
            window.onload = function()
            {
                website.about.animation();
            };
        </script>
    </body>
</html>