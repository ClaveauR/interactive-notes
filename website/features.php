<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Interactive Notes</title>
        <link rel="icon" type="image/png" href="public/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="public/css/general.css" />
        <link rel="stylesheet" type="text/css" href="public/css/index.css" />
        <link rel="stylesheet" type="text/css" href="public/css/header.css" />
        <link rel="stylesheet" type="text/css" href="public/css/main.css" />
        <link rel="stylesheet" type="text/css" href="public/css/section_features.css" />
        <script type="text/javascript" src="public/js/core.js"></script>
        <script type="text/javascript" src="public/js/min.js"></script>
    </head>

    <body>
        <header>
            <div>
                <span><a href="index.php">Accueil</a></span>
                <span><a href="about.php">Notre vision</a></span>
                <span class="current"><a href="features.php">Fonctionnalités</a></span>
            </div>
            <div>
            </div>
            <div>
                <span><a href="donate.php">Faire un don</a></span>
                <span><a href="versions.php">Versions</a></span>
                <span><a href="login.php">Se connecter</a></span>
            </div>
        </header>

        <section id="page1">
            <span>
                <h1>
                    <img class="quote_up" src="public/images/quote_up.png" /><br />
                    Pour les rendre <b>plus percutants</b>, ajoutez<br />Simplement, <b>du mouvement</b> dans vos documents.
                    <img class="quote_down" src="public/images/quote_down.png" />
                </h1>
                Avec Interactive Notes, allez au-delà des documents statiques et ajoutez du dynamisme simplement en intégrant des modules interactifs.
            </span>
        </section>

        <div id="nextArrow"><span><a href="#page2"><img src="public/images/scroll.png" /></a></span></div>

        <section id="page2">
            <div id="title"><span>Ajoutez de la vie, ajoutez des modules !</span></div>
            <div id="description"><span>Les modules interactifs constituent le cœur d'Interactive Notes. En effet, chaque élément d'un document est représenté par un module. Ainsi, les parties, titres, images, graphiques, etc... sont des éléments indépendants pouvant être entièrement personnalisés et animés.<br /><br />Vous pouvez retrouver la liste des modules disponibles, ainsi que leurs fonctionnalités en <a href="more.php#modules">cliquant ici</a>.</span></div>
            <div id="image"></div>
        </section>

        <section id="page3">
            <div id="title"><span>Vos documents, sous haute sécurité, en permanence</span></div>
            <div id="description"><span>A notre époque, il nous paraît évident que votre vie doit rester privée sur Internet. C'est pour cela que nous prenons un soin tout particulier à garantir la protection de vos données à n'importe quel instant. Ces données comprennent bien sûr vos données personnelles comme votre adresse mail mais aussi le contenu de vos documents.<br /><br />Afin de garantir la sécurité de vos données, nous avons mis en place un <a href="https://openpgpjs.org/">système de chiffrement de bout-en-bout utilisant OpenPGP.js</a>, en complément du <a href="https://en.wikipedia.org/wiki/Transport_Layer_Security">protocole de chiffrement TLS 1.3</a>.<br /><br />Pour plus d'informations, nous vous invitons à <a href="more.php#security">cliquer ici</a>.</span></div>
            <div id="image"></div>
        </section>

        <section id="page4">
            <div id="title"><span>La collaboration, un jeu d'enfants</span></div>
            <div id="description"><span>A l'aide des multiples fonctionnalités de collaboration comme l'édition en temps réel, la conversation instantanée, le gestionnaire de tâches, la collaboration devient un jeu d'enfants. La séparation du document en modules permet de simplifier la répartition des tâches, un gain de temps considérable, pour une qualité de travail accrue.<br /><br />Une fois votre document achevé, vous pouvez choisir de l'exporter comme un fichier statique ou interactif d'un simple clic.<br /><br />Découvrez l'ensemble des applications de collaboration en <a href="more.php#collaboration">cliquant ici</a>.</span></div>
            <div id="image"></div>
        </section>

        <section id="page5">
            <div id="title"><span>Respectueux de l'environnement et de l'humain</span></div>
            <div id="description"><span>Notre projet est bâti sur des valeurs, notamment le respect et le partage. C'est pour cela qu'Interactive Notes est entièrement gratuit, et financé par des dons. Lorsque chaque objectif mensuel est dépassé, les dons sont automatiquement reversés à l'association environnementale <a href="https://www.reforestaction.com/">Reforest'Action</a> qui permet de compenser l'empreinte carbone des serveurs.<br /><br />Nous nous engageons à être le plus transparent possible vis-à-vis de la gestion économique du projet et vous pourrez consulter librement à chaque trimestre, le bilan financier.<br /><br />Finalement, nous croyons en des Internets libres, c'est pour cela qu'Interactive Notes est complètement <a href="https://gitlab.com/ClaveauR/interactive-notes">open-source</a>.</span></div>
            <div id="image"></div>
        </section>
    </body>
</html>