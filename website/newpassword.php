<?php
    if(isset($_GET["token"]) && !empty($_GET["token"]))
    {
        define('APP_RAN', 'APP_RAN');
        
        require_once("core/database.php");
        
        $database = databaseConnection();
        
        $req = $database->prepare("SELECT * FROM newpassword WHERE token = ?");
        $req->execute(array($_GET["token"]));
        $result = $req->fetchAll();
        $req->closeCursor();
        
        if(count($result) == 1)
        {
            $token = $_GET["token"];
        }
        else
        {
            die("Le jeton de redéfinition de mot de passe est invalide.");
        }
    }
    else
    {
        die("Le jeton de redéfinition de mot de passe est invalide.");
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Interactive Notes</title>
        <link rel="icon" type="image/png" href="public/images/logo.png" />
        <link rel="stylesheet" type="text/css" href="public/css/general.css" />
        <link rel="stylesheet" type="text/css" href="public/css/index.css" />
        <link rel="stylesheet" type="text/css" href="public/css/header.css" />
        <link rel="stylesheet" type="text/css" href="public/css/main.css" />
        <link rel="stylesheet" type="text/css" href="public/css/section_about.css" />
        <link rel="stylesheet" type="text/css" href="public/css/form.css" />
        <script type="text/javascript" src="public/js/core.js"></script>
        <script type="text/javascript" src="public/js/min.js"></script>
        <script type="text/javascript" src="public/js/librairies/sha512.js"></script>
    </head>

    <body>
        <header>
            <div>
                <span><a href="index.php">Accueil</a></span>
                <span><a href="about.php">Notre vision</a></span>
                <span><a href="features.php">Fonctionnalités</a></span>
            </div>
            <div>
            </div>
            <div>
                <span><a href="donate.php">Faire un don</a></span>
                <span><a href="versions.php">Versions</a></span>
                <span class="current"><a href="login.php">Se connecter</a></span>
            </div>
        </header>

        <main>
            <span>Redéfinissez votre mot de passe<b>|</b></span>
        </main>

        <section>
            <span>
                Le contenu de vos documents est chiffré avec votre mot de passe.<br /><b>Sa réinitialisation entrainera la perte de l'ensemble de vos projets privés.</b><br /><br /><br />
                <input type="password" placeholder="Entrez votre nouveau mot de passe" /><br /><br />
                <input type="password" placeholder="Retapez votre nouveau mot de passe" /><br /><br />
                <input type="button" value="Valider" onclick="website.login.newPassword('<?= $token; ?>');" />
                <p></p>
            </span>
        </section>

        <script type="text/javascript">
            window.onload = function()
            {
                website.about.animation();
            };
        </script>
    </body>
</html>