"use_strict";

var website =
{
    index:
    {
        animation: function()
        {
            var words = [
                "documents",
                "articles",
                "cours",
                "présentations",
                "notes"
            ];

            var colors = [
                "#D737AA",
                "#0afc6b",
                "#b70afc",
                "#3BD82C",
                "#fcaf0a"
            ];

            var currentIndex = 0;
            var currentAction = "write";
            var currentWordIndex = words[currentIndex].length - 1;

            // Animation du curseur
            setInterval(function(){
                if($("main b").style.opacity == 1)
                {
                    $("main b").style.opacity = 0;
                }
                else
                {
                    $("main b").style.opacity = 1;
                }
            }, 500);

            setInterval(function(){
                if(currentAction == "write")
                {
                    if(currentWordIndex >= words[currentIndex].length-1)
                    {
                        currentAction = "delete";

                        var color = colors[currentIndex];
                        $("section").style.backgroundSize = "cover";

                        $("section div")[currentIndex].style.opacity = 1;

                        for(let i = 0; i < $("section div").length; i++)
                        {
                            if(i != currentIndex) $("section div")[i].style.opacity = 0;
                        }

                        $("main i").style.color = color;
                        $("main i").style.borderBottom = "1px solid " + color;
                    }
                    else
                    {
                        currentWordIndex += 1;

                        if(words[currentIndex][currentWordIndex] != undefined) $("main i").innerHTML += words[currentIndex][currentWordIndex];

                        $("main i").style.color = "black";
                        $("main i").style.borderBottom = "1px solid transparent";
                    }
                }
                else
                {
                    if(currentWordIndex < 0)
                    {
                        currentAction = "write";

                        currentIndex += 1;

                        if(currentIndex > words.length-1) currentIndex = 0;
                    }
                    else
                    {
                        setTimeout(function(){
                            currentWordIndex -= 1;

                            $("main i").innerHTML = $("main i").innerHTML.slice(0, -1);

                            $("main i").style.color = "black";
                            $("main i").style.borderBottom = "1px solid transparent";
                        }, 3000);
                    }
                }
            }, 75);
        }
    },
    about:
    {
        animation: function()
        {
            // Animation du curseur
            setInterval(function(){
                if($("main b").style.opacity == 1)
                {
                    $("main b").style.opacity = 0;
                }
                else
                {
                    $("main b").style.opacity = 1;
                }
            }, 500);
        }
    },
    login:
    {
        identify: function()
        {
            var mail = $("input")[0].value;
            var password = sha512($("input")[1].value);

            $("p").className = "waiting";
            $("p").innerHTML = "<br /><img src='public/images/login_waiting.png' />&nbsp;Authentification en cours...";

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "core/identify.php", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("mail=" + mail + "&password=" + password);

            xhr.onreadystatechange = function()
            {
                if(xhr.status == 200 && xhr.readyState == 4)
                {
                    if(xhr.responseText == "OK")
                    {
                        $("p").className = "success";
                        $("p").innerHTML = "<br /><img src='public/images/login_success.png' />&nbsp;Vous êtes maintenant connecté. Vous allez être redirigé dans quelques instants.";

                        setTimeout(function(){
                            document.location.href = "../editor/index.php";
                        }, 5000);
                    }
                    else
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Une erreur s'est produite lors de l'authentification.";
                    }
                }
            };
        },
        register: function()
        {
            var mail = $("input")[0].value;
            var password = sha512($("input")[1].value);
            var password_confirm = sha512($("input")[2].value);

            $("p").className = "waiting";
            $("p").innerHTML = "<br /><img src='public/images/login_waiting.png' />&nbsp;Création de votre compte en cours...";

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "core/register.php", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("mail=" + mail + "&password=" + password + "&password_confirm=" + password_confirm);

            xhr.onreadystatechange = function()
            {
                if(xhr.status == 200 && xhr.readyState == 4)
                {
                    if(xhr.responseText == "OK")
                    {
                        $("p").className = "success";
                        $("p").innerHTML = "<br /><img src='public/images/login_success.png' />&nbsp;Votre compte a été créé avec succès. Vous allez êtes redirigé dans quelques instants.";

                        setTimeout(function(){
                            document.location.href = "../editor/index.php";
                        }, 5000);
                    }
                    else if(xhr.responseText == "MAIL_FORMAT")
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Le format de l'adresse mail est invalide.";
                    }
                    else if(xhr.responseText == "MAIL_EXISTS")
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Cette adresse mail est déjà associée à un compte existant.";
                    }
                    else if(xhr.responseText == "PASSWORD_DIFFERENT")
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Les mots de passe sont différents.";
                    }
                    else if(xhr.responseText == "REGISTER_CLOSE")
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Les inscriptions ne sont pas encore ouverts.";
                    }
                    else
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Une erreur s'est produite lors de la création de votre compte.";
                    }
                }
            };
        },
        lostPassword: function()
        {
            var mail = $("input")[0].value;
    
            $("p").className = "waiting";
            $("p").innerHTML = "<br /><img src='public/images/login_waiting.png' />&nbsp;Traitement en cours...";

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "core/lostpassword.php", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("mail=" + mail);

            xhr.onreadystatechange = function()
            {
                if(xhr.status == 200 && xhr.readyState == 4)
                {
                    if(xhr.responseText == "OK")
                    {
                        $("p").className = "success";
                        $("p").innerHTML = "<br /><img src='public/images/login_success.png' />&nbsp;Un mail vous a été envoyé afin de redéfinir votre mot de passe.";
                    }
                    else
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Une erreur s'est produite.";
                    }
                }
            };
        },
        newPassword: function(token)
        {
            var password = sha512($("input")[0].value);
            var password_confirm = sha512($("input")[1].value);

            $("p").className = "waiting";
            $("p").innerHTML = "<br /><img src='public/images/login_waiting.png' />&nbsp;Traitement en cours...";

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "core/newpassword.php", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send("token=" + token + "&password=" + password + "&password_confirm=" + password_confirm);

            xhr.onreadystatechange = function()
            {
                if(xhr.status == 200 && xhr.readyState == 4)
                {
                    if(xhr.responseText == "OK")
                    {
                        $("p").className = "success";
                        $("p").innerHTML = "<br /><img src='public/images/login_success.png' />&nbsp;Votre mot de passe a été redéfini avec succès. Vous allez être redirigé dans quelques instants.";

                        setTimeout(function(){
                            document.location.href = "login.php";
                        }, 5000);
                    }
                    else if(xhr.responseText == "PASSWORD_DIFFERENT")
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Les mots de passe sont différents.";
                    }
                    else
                    {
                        $("p").className = "error";
                        $("p").innerHTML = "<br /><img src='public/images/login_error.png' />&nbsp;Une erreur s'est produite lors de la redéfinition de votre mot de passe.";
                    }
                }
            };
        }
    }
};