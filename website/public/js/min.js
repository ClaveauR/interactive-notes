$ = function(arg)
{
    if(typeof arg == "object")
    {
        return arg;
    }
    
    if(document.querySelectorAll(arg) != undefined)
    {
        if(document.querySelectorAll(arg).length == 1)
        {
            return document.querySelector(arg);
        }
        
        return document.querySelectorAll(arg);
    }
    
    return document.querySelector("body");
}